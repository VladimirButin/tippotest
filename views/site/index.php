<?php

/* @var $this yii\web\View */
use dosamigos\fileupload\FileUpload;
use yii\bootstrap\Button;

$this->title = 'Test project';
?>
<div class="site-index">
    <div class="col-md-6 col-xs-12">
        <img id="original" src="" alt="">
        <?= FileUpload::widget([
            'model' => $model,
            'attribute' => 'image',
            'url' => ['site/upload'],
            'options' => [
                'accept' => 'image/*'
            ],
            'clientOptions' => [
                'maxFileSize' => 2000000
            ],
            'clientEvents' => [
                'fileuploaddone' => 'function(e, data) {
                    $(document).trigger("imageUploaded", data);
                }',
            ]
        ]); ?>
    </div>
    <div class="col-md-6 col-xs-12">
        <img id="product" src="" alt="">
        <?= Button::widget([
            'label' => 'Make better',
            'options' => [
                'id' => 'makeBetter',
                'class' => ['btn-primary']
            ]
        ]); ?>
    </div>
</div>
