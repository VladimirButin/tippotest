$(function () {
    var $original = $('#original'),
        $product = $('#product'),
        $makeBetter = $('#makeBetter');

    $(document).on('imageUploaded', function (e, data) {
        let result = JSON.parse(data.result);
        if (result && result.file) {
            $original.attr('src', result.file.url);
        }
    });

    $makeBetter.click(function () {
        $.ajax({
            'method': 'GET',
            'url': '/site/betterize',
            'dataType': 'json'
        }).then(function (data) {
            console.dir(arguments);
            if (data && data.file) {
                $product.attr('src', data.file.url);
            }
        })
    })
});
