<?php
/**
 * Created by PhpStorm.
 * User: olre
 * Date: 31.08.16
 * Time: 20:49
 */

namespace app\models;

use yii\base\Model;

class Image extends Model
{
    public $image;

    public function rules()
    {
        return [
            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, gif, bmp'],
        ];
    }
}