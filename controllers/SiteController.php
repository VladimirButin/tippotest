<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use app\models\Image;
use Imagick;

class SiteController extends Controller
{
    const IMAGE_SESSION_NAME = 'imageName';
    const EXTENSION_SESSION_NAME = 'extension';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'model' => new Image(),
        ]);
    }

    /**
     * Get directory path to user images
     *
     * @return string
     */
    private function getDirectoryPath()
    {
        return Yii::getAlias('@webroot/tmp') . DIRECTORY_SEPARATOR . Yii::$app->session->id . DIRECTORY_SEPARATOR;
    }

    /**
     * Get url path to user images
     *
     * @return string
     */
    private function getUrlPath()
    {
        return '/tmp/' . Yii::$app->session->id . '/';
    }

    /**
     * Uploads image
     *
     * @return string
     */
    public function actionUpload()
    {
        Yii::$app->session->open();
        $imageFile = UploadedFile::getInstanceByName('Image[image]');
        $directory = $this->getDirectoryPath();
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }

        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $uid . '.' . $imageFile->extension;
            $filePath = $directory . $fileName;

            if ($imageFile->saveAs($filePath)) {
                $url = $this->getUrlPath() . $fileName;
                Yii::$app->session->set(self::IMAGE_SESSION_NAME, $fileName);
                Yii::$app->session->set(self::EXTENSION_SESSION_NAME, $imageFile->extension);

                return Json::encode([
                    'file' => [
                        'name' => $fileName,
                        'size' => $imageFile->size,
                        'url' => $url,
                        'thumbnailUrl' => $url,
                    ]
                ]);
            }
        }

        return '';
    }

    /**
     * Betterize image
     */
    public function actionBetterize()
    {
        if ($fileName = Yii::$app->session->get(self::IMAGE_SESSION_NAME)) {
            $directory = $this->getDirectoryPath();
            $filePath = $directory . $fileName;

            $image = new Imagick($filePath);
            $image->autoLevelImage(Imagick::CHANNEL_ALL);
            $image->contrastImage(true);
            $image->modulateImage(106, 150, 100);

            $uid = uniqid(time(), true);
            $resultFileName = $uid . '_result.' . Yii::$app->session->get(self::EXTENSION_SESSION_NAME);
            $resultFilePath = $directory . $resultFileName;
            if ($image->writeImage($resultFilePath)) {
                $url = $this->getUrlPath() . $resultFileName;
                return Json::encode([
                    'file' => [
                        'url' => $url,
                    ]
                ]);
            }
        }
    }

}
